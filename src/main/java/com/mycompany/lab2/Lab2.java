/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;
import static com.mycompany.lab2.Lab2.currentPlayer;
import java.util.Scanner;
/**
 *
 * @author Frame
 */
public class Lab2 {
    static int row, col;
    static int isDraw = 1;
    static boolean isWin = false;
    static String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    static String currentPlayer = "X";
    public static void main(String[] args) {
        printWelcome();
        printTable();
        while (true) {
            printTurn();
            inputRowCol();
            printTable();
            checkWin();
            if (isWin) {
                printTable();
                printWin();
                break;
            } else if (isDraw == 9) {
                System.out.println("Draw!!!");
                break;
            }
            swichPlayer();
            isDraw++;
        }
    }
    public static void printWelcome() {
        System.out.println("Welcome to XO");
    }
    public static void printTurn() {
        System.out.println(currentPlayer + " Turn ");
    }
    public static void printTable() {
        System.out.println("");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");    
            }
            System.out.println();
        }
    }
    public static void printWin() {
        System.out.println(currentPlayer + " Win!!! ");
    }
    public static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == "-") {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }
    public static void swichPlayer() {
        if (currentPlayer == "X") {
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }
    public static void checkWin() {
        if (table[0][0] == "X" && table[0][1] == "X" && table[0][2] == "X") {
            isWin = true;
        } else if (table[0][0] == "O" && table[0][1] == "O" && table[0][2] == "O") {
            isWin = true;
        } else if (table[0][0] == "X" && table[1][0] == "X" && table[2][0] == "X") {
            isWin = true;
        } else if (table[0][0] == "O" && table[1][0] == "O" && table[2][0] == "O") {
            isWin = true;
        } else if (table[0][1] == "X" && table[1][1] == "X" && table[2][1] == "X") {
            isWin = true;
        } else if (table[0][1] == "O" && table[1][1] == "O" && table[2][1] == "O") {
            isWin = true;
        } else if (table[0][2] == "X" && table[1][2] == "X" && table[2][2] == "X") {
            isWin = true;
        } else if (table[0][2] == "O" && table[1][2] == "O" && table[2][2] == "O") {
            isWin = true;
        } else if (table[1][0] == "X" && table[1][1] == "X" && table[1][2] == "X") {
            isWin = true;
        } else if (table[1][0] == "O" && table[1][1] == "O" && table[1][2] == "O") {
            isWin = true;
        } else if (table[2][0] == "X" && table[2][1] == "X" && table[2][2] == "X") {
            isWin = true;
        } else if (table[2][0] == "O" && table[2][1] == "O" && table[2][2] == "O") {
            isWin = true;
        } else if (table[0][0] == "X" && table[1][1] == "X" && table[2][2] == "X") {
            isWin = true;
        } else if (table[0][0] == "O" && table[1][1] == "O" && table[2][2] == "O") {
            isWin = true;
        }
    }

}
